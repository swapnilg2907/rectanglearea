package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class RectangleTest {

    @Test
    void calculateArea() {
        assertEquals(12, new Rectangle(3,4).area());
    }

    @Test
    void calculatePerimeter() {
        assertEquals(18, new Rectangle(4,5).perimeter());
    }
   @Test
     void calculateBigger(){
        assertTrue(new Rectangle(8,5).isBigger(new Rectangle(5,6)));
    }
    @Test
      void squareArea(){
        //Rectangle rectangle = new Rectangle.Square(4);
        assertEquals(16, (Rectangle.CreateSquare(4)).area());
    }
    @Test
    void squarePerimeter(){
        assertEquals(20, Rectangle.CreateSquare(5).perimeter());
    }
}
