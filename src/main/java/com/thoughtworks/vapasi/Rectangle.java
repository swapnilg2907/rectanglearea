package com.thoughtworks.vapasi;

public class Rectangle {

    private int length;
    private int breadth;

    public Rectangle(int side) {
        length = side;
        breadth = side;
    }

    public Rectangle(int length, int breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    public float area() {
        return length * breadth;
    }

    public float perimeter() {
        return 2 * (length + breadth);
    }

    public boolean isBigger(Rectangle anotherRectangle) {
        return this.area() > anotherRectangle.area();
    }

    public static Rectangle CreateSquare(int side){
        return new Rectangle(side);
    }

}
